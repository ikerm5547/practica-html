// Función para cargar las tarjetas de automóviles según la marca seleccionada
function cargarTarjetasPorMarca(automovilesFiltrados) {
    const tarjetasContainer = document.getElementById("tarjetasContainer");
    // Limpiar el contenedor de tarjetas
    tarjetasContainer.innerHTML = "";

    // Crear y agregar tarjetas de automóviles filtrados al contenedor
    automovilesFiltrados.forEach(automovil => {
        const card = document.createElement("div");
        card.classList.add("card");

        const contenedorFlex = document.createElement("div");
        contenedorFlex.classList.add("contenedorFlex");

        const imagen = document.createElement("img");
        imagen.classList.add("img");
        imagen.src = automovil.imagen;
        imagen.alt = automovil.modelo;
        imagen.width = "100%";

        const h3 = document.createElement("h3");
        h3.textContent = automovil.modelo;

        const pMarca = document.createElement("p");
        pMarca.classList.add("title");
        pMarca.textContent = automovil.marca;

        const pDescripcion = document.createElement("p");
        pDescripcion.classList.add("anta-regular");
        pDescripcion.textContent = automovil.descripcion;

        const boton = document.createElement("button");
        boton.textContent = "Ver Ficha Técnica";

        contenedorFlex.appendChild(imagen);
        contenedorFlex.appendChild(h3);
        contenedorFlex.appendChild(pMarca);
        contenedorFlex.appendChild(pDescripcion);
        contenedorFlex.appendChild(boton);
        card.appendChild(contenedorFlex);
        tarjetasContainer.appendChild(card);
    });
}

const automoviles = [
    {
        marca: "BMW",
        modelo: "M3",
        descripcion: "BMW M3 sedán deportivo de alto rendimiento que ofrece una combinación de potencia, elegancia y tecnología. Equipado con un motor de seis cilindros en línea turboalimentado, tracción trasera y una transmisión manual o automática de doble embrague, el M3 ofrece una experiencia de conducción emocionante y ágil.",
        imagen: "/IMG/m3.jpg"
    },
    {
        marca: "BMW",
        modelo: "X6",
        descripcion: " El BMW X6 es un SUV deportivo de lujo con un diseño distintivo, potente rendimiento y tecnología avanzada de vanguardia.",
        imagen: "/IMG/bmwx6.png"
    },
    {
        marca: "MERCEDES",
        modelo: "Class A",
        descripcion: "MERCEDES Class A es un modelo de lujo que ofrece un rendimiento excepcional y una elegancia distintiva. Diseñado con los más altos estándares de ingeniería y estilo, el Class A es una opción preferida para aquellos que buscan una combinación de comodidad y prestaciones.",
        imagen: "/IMG/mercedez.jpg"
    },
    {
        marca: "MERCEDES",
        modelo: "AMG",
        descripcion: "El Mercedes-AMG GT es una obra maestra de ingeniería y diseño que combina elegancia con un rendimiento excepcional. Este automóvil deportivo de lujo captura la esencia de la marca AMG, conocida por su potencia y estilo distintivos.",
        imagen: "/IMG/AMG Gt.png"
    },
    {
        marca: "NISSAN",
        modelo: "Sentra",
        descripcion: "NISSAN Sentra es un sedán compacto que ofrece un equilibrio entre comodidad, economía de combustible y tecnología. Diseñado para satisfacer las necesidades de los conductores urbanos, el Sentra ofrece un diseño elegante y una conducción suave.",
        imagen: "/IMG/nissan sentra.png"
    },
    {
        marca: "NISSAN",
        modelo: "Versa",
        descripcion: "El Nissan Versa es un sedán compacto que ha sido una opción popular en el mercado durante décadas gracias a su combinación de confiabilidad, economía y comodidad. El modelo 2018 continúa esta tradición con estilo y practicidad.",
        imagen: "/IMG/nissan versa.png"
    }
];


// Obtener el elemento select
const selectMarca = document.getElementById("selMarc");

// Agregar evento de cambio al select
selectMarca.addEventListener("change", function() {
    // Obtener el valor seleccionado
    const selectedMarca = this.value;
    
    console.log(automoviles); // Verifica si automoviles está definido correctamente
const automovilesFiltrados = automoviles.filter(auto => auto.marca === selectedMarca);

    // Llamar a la función para cargar las tarjetas de automóviles filtradas por la marca seleccionada
    cargarTarjetasPorMarca(automovilesFiltrados);
});
