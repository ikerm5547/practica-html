let otroArreglo = []; // Inicializamos el arreglo vacío

// Función para llenar un arreglo con números aleatorios
function llenarArray(tamanio){
    otroArreglo = []; // Limpiamos el arreglo
    for (let i = 0; i < tamanio; ++i) {
        otroArreglo.push(Math.floor(Math.random() * 100)); // Genera un número aleatorio entre 0 y 99
    }
}

// Función para limpiar los resultados
function limpiar(){
    document.getElementById("llenarArray").innerHTML = "";
    document.getElementById("numPares").innerHTML = "";
    document.getElementById("numImpares").innerHTML = "";
}

// Función para contar la cantidad de números pares en un arreglo
function numPares(otroArray){
    let pares = 0;
    for (let i = 0; i < otroArray.length; ++i) {
        if (otroArray[i] % 2 === 0) {
            pares++;
        }
    }
    return pares;
}

// Función para contar la cantidad de números impares en un arreglo
function numImpares(otroArray){
    let impares = 0;
    for (let i = 0; i < otroArray.length; ++i) {
        if (otroArray[i] % 2 !== 0) {
            impares++;
        }
    }
    return impares;
} 

// Función para verificar si un arreglo es simétrico o asimétrico
// Función para verificar si la cantidad de números pares e impares es simétrica o asimétrica
function verificarSimetriaParesImpares(otroArray) {
    let numPares = 0;
    let numImpares = 0;

    // Contar la cantidad de números pares e impares en el arreglo
    for (let i = 0; i < otroArray.length; i++) {
        if (otroArray[i] % 2 === 0) {
            numPares++;
        } else {
            numImpares++;
        }
    }

    // Verificar si la cantidad de pares e impares es igual
    if (numPares === numImpares) {
        return "El arreglo es simétrico.";
    } else {
        return "El arreglo es asimétrico.";
    }
}


function mostrarResultado() {
    const tamanio = parseInt(document.getElementById('tamanio').value);
    if (isNaN(tamanio) || tamanio <= 0) {
        alert("Por favor, ingrese un número válido para el tamaño del arreglo.");
        return;
    }
    llenarArray(tamanio);
    let llenadoAleatorio = "Arreglo llenado aleatoriamente: " + otroArreglo.join(", ");
    let numParesEnArreglo = "Cantidad de números pares: " + numPares(otroArreglo);
    let numImparesEnArreglo = "Cantidad de números impares: " + numImpares(otroArreglo);
    let simetriaArreglo = verificarSimetriaParesImpares(otroArreglo); // Verificar simetría por cantidad de pares e impares

    document.getElementById("llenarArray").innerHTML = llenadoAleatorio;
    document.getElementById("numPares").innerHTML = numParesEnArreglo;
    document.getElementById("numImpares").innerHTML = numImparesEnArreglo;
    document.getElementById("simetria").innerHTML = simetriaArreglo; // Mostrar simetría
}
