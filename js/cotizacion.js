document.getElementById('cotizacionForm').addEventListener('submit', function(event) {
    event.preventDefault();
    
    // Obtener los valores del formulario
    const tipo = document.getElementById('tipo').value;
    const nombre = document.getElementById('nombre').value;
    const marca = document.getElementById('marca').value;
    const modelo = document.getElementById('modelo').value;
    const anio = parseInt(document.getElementById('anio').value);
    const precio = parseInt(document.getElementById('precio').value);
    const pagoInicialPorcentaje = parseInt(document.getElementById('pagoInicial').value) / 100;
    const meses = parseInt(document.getElementById('meses').value);
    const nombreCliente = document.getElementById('nombreCliente').value;
    const email = document.getElementById('email').value;
    const telefono = document.getElementById('telefono').value;
    const comentarios = document.getElementById('comentarios').value;

    // Calcular el pago inicial en dinero
    const pagoInicial = precio * pagoInicialPorcentaje;

    // Calcular el impuesto
    const impuestos = precio * 0.05;

    // Calcular el subtotal
    const subtotal = precio - pagoInicial + impuestos;

    // Calcular el pago mensual
    const tasaInteres = 0.1; // Tasa de interés del 10%
    const pagoMensual = (subtotal * tasaInteres) / meses;

    // Calcular el total
    const total = subtotal + (pagoMensual * meses);

    // Mostrar la cotización en el div cotizacionResult
    const cotizacionHTML = `
        <h2>Cotización:</h2>
        <p>Vehículo: ${nombre}</p>
        <p>Marca: ${marca}</p>
        <p>Modelo: ${modelo}</p>
        <p>Año: ${anio}</p>
        <p>Precio: $${precio}</p>
        <p>Pago Inicial: $${pagoInicial}</p>
        <p>Impuestos: $${impuestos}</p>
        <p>Meses: ${meses} meses</p>
        <p>Pago Mensual: $${pagoMensual.toFixed(2)}</p>
        <p>Total: $${total.toFixed(2)}</p>
        <h3>Información del Cliente:</h3>
        <p>Nombre: ${nombreCliente}</p>
        <p>Email: ${email}</p>
        <p>Teléfono: ${telefono}</p>
        <p>Comentarios: ${comentarios}</p>
    `;
    
    document.getElementById('cotizacionResult').innerHTML = cotizacionHTML;
});
