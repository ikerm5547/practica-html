document.getElementById('imcForm').addEventListener('submit', function(event) {
    event.preventDefault();
    var altura = parseFloat(document.getElementById('altura').value) / 100; // convertir altura a metros
    var peso = parseFloat(document.getElementById('peso').value);
    var imc = peso / (altura * altura);
    var resultado = '';

    if (!isNaN(imc)) {
        if (imc < 18.5) {
            resultado = 'Bajo peso';
        } else if (imc < 25) {
            resultado = 'Peso normal';
        } else if (imc < 30) {
            resultado = 'Sobrepeso';
        } else {
            resultado = 'Obesidad';
        }
        resultado += ' (IMC: ' + imc.toFixed(2) + ')';
    } else {
        resultado = 'Por favor, introduce valores válidos.';
    }

    alert(resultado); // Muestra el resultado en un mensaje de alerta
});
