// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.11.1/firebase-app.js";
import { getDatabase, onValue, ref as refS, set, get, update, child, remove } from "https://www.gstatic.com/firebasejs/10.11.1/firebase-database.js";
import { getStorage, ref as refStorage, uploadBytesResumable, getDownloadURL } from "https://www.gstatic.com/firebasejs/10.11.1/firebase-storage.js";

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBK0nHrSlFrniSyijCZcXLjT2vniDS3-t0",
  authDomain: "webcrud-b3ccb.firebaseapp.com",
  databaseURL: "https://productos-f77f2-default-rtdb.firebaseio.com/",
  projectId: "webcrud-b3ccb",
  storageBucket: "webcrud-b3ccb.appspot.com",
  messagingSenderId: "825180979694",
  appId: "1:825180979694:web:7ff060c0b1f450d8254eff"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
const storage = getStorage(app);

// Declare global variables
var numSerie = 0;
var marca = "";
var modelo = "";
var descripcion = "";
var urlImag = "";

// Functions
function leerInputs() {
  numSerie = document.getElementById('txtNumSerie').value;
  marca = document.getElementById('txtMarca').value;
  modelo = document.getElementById('txtModelo').value;
  descripcion = document.getElementById('txtDescripcion').value;
  urlImag = document.getElementById('txtUrl').value;
}

function mostrarMensaje(mensaje) {
  var mensajeElement = document.getElementById('mensaje');
  mensajeElement.textContent = mensaje;
  mensajeElement.style.display = 'block';
  setTimeout(() => {
    mensajeElement.style.display = 'none';
  }, 1000);
}

// Upload image to Firebase Storage
const uploadButton = document.getElementById('uploadButton');
uploadButton.addEventListener('click', uploadImage);

function uploadImage(event) {
  event.preventDefault();
  const imageInput = document.getElementById('imageInput');
  const file = imageInput.files[0];
  if (!file) {
    mostrarMensaje("No se seleccionó ninguna imagen");
    return;
  }

  const storageRef = refStorage(storage, 'images/' + file.name);
  const uploadTask = uploadBytesResumable(storageRef, file);

  uploadTask.on('state_changed', 
    (snapshot) => {
      // Progress function
      const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
      document.getElementById('progress').innerText = 'Progreso: ' + progress + '%';
    }, 
    (error) => {
      mostrarMensaje("Error al subir la imagen: " + error.message);
    }, 
    () => {
      // Complete function
      getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
        document.getElementById('txtUrl').value = downloadURL;
        mostrarMensaje("Imagen subida con éxito");
      });
    }
  );
}

// Add product to the database
const btnAgregar = document.getElementById('btnAgregar');
btnAgregar.addEventListener('click', insertarProducto);

function insertarProducto() {
  leerInputs();
  // Validate
  if (numSerie === "" || marca === "" || modelo === "" || descripcion === "") {
    mostrarMensaje("Faltaron datos por capturar");
    return;
  }
  set(refS(db, 'Automoviles/' + numSerie), {
    // Data to save
    numSerie: numSerie,
    marca: marca,
    modelo: modelo,
    descripcion: descripcion,
    urlImag: urlImag
  }).then(() => {
    alert("Se agregó con éxito");
  }).catch((error) => {
    alert("Ocurrió un error: " + error);
  });
  listarProductos();
}

function listarProductos() {
  const dbRef = refS(db, 'Automoviles');
  const tabla = document.getElementById('tablaProductos');
  const tbody = tabla.querySelector('tbody');
  tbody.innerHTML = '';
  onValue(dbRef, (snapshot) => {
    snapshot.forEach((childSnapshot) => {
      const childKey = childSnapshot.key;
      const data = childSnapshot.val();
      var fila = document.createElement('tr');
      var celdaCodigo = document.createElement('td');
      celdaCodigo.textContent = childKey;
      fila.appendChild(celdaCodigo);
      var celdaNombre = document.createElement('td');
      celdaNombre.textContent = data.marca;
      fila.appendChild(celdaNombre);
      var celdaPrecio = document.createElement('td');
      celdaPrecio.textContent = data.modelo;
      fila.appendChild(celdaPrecio);
      var celdaCantidad = document.createElement('td');
      celdaCantidad.textContent = data.descripcion;
      fila.appendChild(celdaCantidad);
      var celdaImagen = document.createElement('td');
      var imagen = document.createElement('img');
      imagen.src = data.urlImag;
      imagen.width = 100;
      celdaImagen.appendChild(imagen);
      fila.appendChild(celdaImagen);
      tbody.appendChild(fila);
    });
  });
}

function buscarAutomovil() {
  numSerie = document.getElementById('txtNumSerie').value.trim();
  if (numSerie === "") {
    mostrarMensaje("Faltó capturar el número de serie");
    return;
  }
  const dbref = refS(db);
  get(child(dbref, 'Automoviles/' + numSerie)).then((snapshot) => {
    if (snapshot.exists()) {
      marca = snapshot.val().marca;
      modelo = snapshot.val().modelo;
      descripcion = snapshot.val().descripcion;
      urlImag = snapshot.val().urlImag;
      escribirInputs();
    } else {
      limpiarInputs();
      mostrarMensaje("No se encontró el registro");
    }
  });
}

function actualizarAutomovil() {
  leerInputs();
  if (numSerie === "" || marca === "" || modelo === "" || descripcion === "") {
    mostrarMensaje("Favor de capturar toda la información.");
    return;
  }
  update(refS(db, 'Automoviles/' + numSerie), {
    numSerie: numSerie,
    marca: marca,
    modelo: modelo,
    descripcion: descripcion,
    urlImag: urlImag
  }).then(() => {
    mostrarMensaje("Se actualizó con éxito.");
    limpiarInputs();
    listarProductos();
  }).catch((error) => {
    mostrarMensaje("Ocurrió un error: " + error);
  });
  listarProductos();
}

function eliminarAutomovil() {
  let numSerie = document.getElementById('txtNumSerie').value.trim();
  if (numSerie === "") {
    mostrarMensaje("No se ingresó un número de serie válido.");
    return;
  }
  const dbref = refS(db);
  get(child(dbref, 'Automoviles/' + numSerie)).then((snapshot) => {
    if (snapshot.exists()) {
      remove(refS(db, 'Automoviles/' + numSerie)).then(() => {
        mostrarMensaje("Producto eliminado con éxito.");
        limpiarInputs();
        listarProductos();
      }).catch((error) => {
        mostrarMensaje("Ocurrió un error al eliminar el producto: " + error);
      });
    } else {
      limpiarInputs();
      mostrarMensaje("El producto con ID " + numSerie + " no existe.");
    }
  });
  listarProductos();
}

function escribirInputs() {
  document.getElementById('txtMarca').value = marca;
  document.getElementById('txtModelo').value = modelo;
  document.getElementById('txtDescripcion').value = descripcion;
  document.getElementById('txtUrl').value = urlImag;
}

function limpiarInputs() {
  document.getElementById('txtNumSerie').value = '';
  document.getElementById('txtMarca').value = '';
  document.getElementById('txtModelo').value = '';
  document.getElementById('txtDescripcion').value = '';
  document.getElementById('txtUrl').value = '';
}

// Event listeners
const btnBuscar = document.getElementById('btnBuscar');
btnBuscar.addEventListener('click', buscarAutomovil);

const btnBorrar = document.getElementById('btnBorrar');
btnBorrar.addEventListener('click', eliminarAutomovil);

const btnActualizar = document.getElementById('btnActualizar');
btnActualizar.addEventListener('click', actualizarAutomovil);

window.onload = listarProductos;
